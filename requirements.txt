numpy
pandas
scipy
sympy
statsmodels
numdifftools
scikit-learn>=0.24.2 # not directly required, pinned by Snyk to avoid a vulnerability